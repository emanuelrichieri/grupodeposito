﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DACS.Distribuidora.Entities;
using ServerDeposito;

namespace ServerDeposito.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductZonesController : ControllerBase
    {
        private readonly DepositoContext _context;

        public ProductZonesController(DepositoContext context)
        {
            _context = context;
        }

        // GET: api/ProductZones
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductZone>>> GetProductZones()
        {
            return await _context.ProductZones.ToListAsync();
        }

        // GET: api/ProductZones/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductZone>> GetProductZone(long id)
        {
            var productZone = await _context.ProductZones.FindAsync(id);

            if (productZone == null)
            {
                return NotFound();
            }

            return productZone;
        }

        // PUT: api/ProductZones/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductZone(long id, ProductZone productZone)
        {
            if (id != productZone.Id)
            {
                return BadRequest();
            }

            _context.Entry(productZone).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductZoneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ProductZones
        [HttpPost]
        public async Task<ActionResult<ProductZone>> PostProductZone(ProductZone productZone)
        {
            _context.ProductZones.Add(productZone);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductZone", new { id = productZone.Id }, productZone);
        }

        // DELETE: api/ProductZones/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductZone>> DeleteProductZone(long id)
        {
            var productZone = await _context.ProductZones.FindAsync(id);
            if (productZone == null)
            {
                return NotFound();
            }

            _context.ProductZones.Remove(productZone);
            await _context.SaveChangesAsync();

            return productZone;
        }

        private bool ProductZoneExists(long id)
        {
            return _context.ProductZones.Any(e => e.Id == id);
        }
    }
}
