﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DACS.Distribuidora.Entities;
using ServerDeposito;

namespace ServerDeposito.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MinimalExistencesController : ControllerBase
    {
        private readonly DepositoContext _context;

        public MinimalExistencesController(DepositoContext context)
        {
            _context = context;
        }

        // GET: api/MinimalExistences
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MinimalExistence>>> GetMinimalExistences()
        {
            return await _context.MinimalExistences.ToListAsync();
        }

        // GET: api/MinimalExistences/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MinimalExistence>> GetMinimalExistence(long id)
        {
            var minimalExistence = await _context.MinimalExistences.FindAsync(id);

            if (minimalExistence == null)
            {
                return NotFound();
            }

            return minimalExistence;
        }

        // PUT: api/MinimalExistences/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMinimalExistence(long id, MinimalExistence minimalExistence)
        {
            if (id != minimalExistence.Id)
            {
                return BadRequest();
            }

            _context.Entry(minimalExistence).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MinimalExistenceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/MinimalExistences
        [HttpPost]
        public async Task<ActionResult<MinimalExistence>> PostMinimalExistence(MinimalExistence minimalExistence)
        {
            _context.MinimalExistences.Add(minimalExistence);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMinimalExistence", new { id = minimalExistence.Id }, minimalExistence);
        }

        // DELETE: api/MinimalExistences/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MinimalExistence>> DeleteMinimalExistence(long id)
        {
            var minimalExistence = await _context.MinimalExistences.FindAsync(id);
            if (minimalExistence == null)
            {
                return NotFound();
            }

            _context.MinimalExistences.Remove(minimalExistence);
            await _context.SaveChangesAsync();

            return minimalExistence;
        }

        private bool MinimalExistenceExists(long id)
        {
            return _context.MinimalExistences.Any(e => e.Id == id);
        }
    }
}
