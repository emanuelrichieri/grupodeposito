﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DACS.Distribuidora.Entities;
using ServerDeposito;

namespace ServerDeposito.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReplenishmentLinesController : ControllerBase
    {
        private readonly DepositoContext _context;

        public ReplenishmentLinesController(DepositoContext context)
        {
            _context = context;
        }

        // GET: api/ReplenishmentLines
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReplenishmentLine>>> GetReplenishmentLines()
        {
            return await _context.ReplenishmentLines.ToListAsync();
        }

        // GET: api/ReplenishmentLines/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReplenishmentLine>> GetReplenishmentLine(long id)
        {
            var replenishmentLine = await _context.ReplenishmentLines.FindAsync(id);

            if (replenishmentLine == null)
            {
                return NotFound();
            }

            return replenishmentLine;
        }

        // PUT: api/ReplenishmentLines/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReplenishmentLine(long id, ReplenishmentLine replenishmentLine)
        {
            if (id != replenishmentLine.Id)
            {
                return BadRequest();
            }

            _context.Entry(replenishmentLine).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReplenishmentLineExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ReplenishmentLines
        [HttpPost]
        public async Task<ActionResult<ReplenishmentLine>> PostReplenishmentLine(ReplenishmentLine replenishmentLine)
        {
            _context.ReplenishmentLines.Add(replenishmentLine);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetReplenishmentLine", new { id = replenishmentLine.Id }, replenishmentLine);
        }

        // DELETE: api/ReplenishmentLines/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ReplenishmentLine>> DeleteReplenishmentLine(long id)
        {
            var replenishmentLine = await _context.ReplenishmentLines.FindAsync(id);
            if (replenishmentLine == null)
            {
                return NotFound();
            }

            _context.ReplenishmentLines.Remove(replenishmentLine);
            await _context.SaveChangesAsync();

            return replenishmentLine;
        }

        private bool ReplenishmentLineExists(long id)
        {
            return _context.ReplenishmentLines.Any(e => e.Id == id);
        }
    }
}
