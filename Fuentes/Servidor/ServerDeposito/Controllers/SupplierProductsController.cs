﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DACS.Distribuidora.Entities;
using ServerDeposito;

namespace ServerDeposito.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierProductsController : ControllerBase
    {
        private readonly DepositoContext _context;

        public SupplierProductsController(DepositoContext context)
        {
            _context = context;
        }

        // GET: api/SupplierProducts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SupplierProduct>>> GetSupplierProducts()
        {
            return await _context.SupplierProducts.ToListAsync();
        }

        // GET: api/SupplierProducts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SupplierProduct>> GetSupplierProduct(long id)
        {
            var supplierProduct = await _context.SupplierProducts.FindAsync(id);

            if (supplierProduct == null)
            {
                return NotFound();
            }

            return supplierProduct;
        }

        // PUT: api/SupplierProducts/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSupplierProduct(long id, SupplierProduct supplierProduct)
        {
            if (id != supplierProduct.Id)
            {
                return BadRequest();
            }

            _context.Entry(supplierProduct).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SupplierProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/SupplierProducts
        [HttpPost]
        public async Task<ActionResult<SupplierProduct>> PostSupplierProduct(SupplierProduct supplierProduct)
        {
            _context.SupplierProducts.Add(supplierProduct);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSupplierProduct", new { id = supplierProduct.Id }, supplierProduct);
        }

        // DELETE: api/SupplierProducts/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SupplierProduct>> DeleteSupplierProduct(long id)
        {
            var supplierProduct = await _context.SupplierProducts.FindAsync(id);
            if (supplierProduct == null)
            {
                return NotFound();
            }

            _context.SupplierProducts.Remove(supplierProduct);
            await _context.SaveChangesAsync();

            return supplierProduct;
        }

        private bool SupplierProductExists(long id)
        {
            return _context.SupplierProducts.Any(e => e.Id == id);
        }
    }
}
