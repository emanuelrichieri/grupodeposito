﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using DACS.Distribuidora.Entities;
using ServerDeposito;

namespace ServerDeposito.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReplenishmentRequestsController : ControllerBase
    {
        private readonly DepositoContext _context;

        public ReplenishmentRequestsController(DepositoContext context)
        {
            _context = context;
        }

        // GET: api/ReplenishmentRequests
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReplenishmentRequest>>> GetReplenishmentRequests()
        {
            return await _context.ReplenishmentRequests.ToListAsync();
        }

        // GET: api/ReplenishmentRequests/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReplenishmentRequest>> GetReplenishmentRequest(long id)
        {
            var replenishmentRequest = await _context.ReplenishmentRequests.FindAsync(id);

            if (replenishmentRequest == null)
            {
                return NotFound();
            }

            return replenishmentRequest;
        }

        // PUT: api/ReplenishmentRequests/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReplenishmentRequest(long id, ReplenishmentRequest replenishmentRequest)
        {
            if (id != replenishmentRequest.Id)
            {
                return BadRequest();
            }

            _context.Entry(replenishmentRequest).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReplenishmentRequestExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/ReplenishmentRequests
        [HttpPost]
        public async Task<ActionResult<ReplenishmentRequest>> PostReplenishmentRequest(ReplenishmentRequest replenishmentRequest)
        {
            _context.ReplenishmentRequests.Add(replenishmentRequest);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetReplenishmentRequest", new { id = replenishmentRequest.Id }, replenishmentRequest);
        }

        // DELETE: api/ReplenishmentRequests/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ReplenishmentRequest>> DeleteReplenishmentRequest(long id)
        {
            var replenishmentRequest = await _context.ReplenishmentRequests.FindAsync(id);
            if (replenishmentRequest == null)
            {
                return NotFound();
            }

            _context.ReplenishmentRequests.Remove(replenishmentRequest);
            await _context.SaveChangesAsync();

            return replenishmentRequest;
        }

        private bool ReplenishmentRequestExists(long id)
        {
            return _context.ReplenishmentRequests.Any(e => e.Id == id);
        }
    }
}
