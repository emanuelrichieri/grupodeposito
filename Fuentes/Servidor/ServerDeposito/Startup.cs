﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using DACS.Distribuidora;
using System.Net;
using System.IO;

using Microsoft.EntityFrameworkCore;
using DACS.Distribuidora.Entities;
using Microsoft.AspNetCore.Http;
using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;

namespace ServerDeposito
{
    public class Startup
    {
        private const string TOKEN_HEADER_NAME = "x-id-token";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddCors(options =>
            {
                options.AddPolicy("AllowMyOrigin",
                builder => builder.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());
            });


            services.AddSingleton((provider) =>
            {
                var firebaseApp = FirebaseApp.Create(new AppOptions()
                {
                    Credential = GoogleCredential.FromFile("privatekey.json"),
                });

                return firebaseApp;
            });

            services.AddDbContext<DepositoContext>(options =>
            options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            //services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors("AllowMyOrigin");

            app.UseHttpsRedirection();

            app.Use(async (context, next) =>
            {
                try
                {
                    await next.Invoke();
                }
                catch (UnauthorizedException ex)
                {
                    context.Response.ContentType = "application/json";
                    context.Response.StatusCode = (int)HttpStatusCode.Unauthorized;

                    using (var writer = new StreamWriter(context.Response.Body))
                    {
                        writer.Write(ex.Message);
                        await writer.FlushAsync().ConfigureAwait(false);
                    }

                }
            });

            app.Use(async (context, next) =>
            {
                var firebaseApp = app.ApplicationServices.GetService<FirebaseApp>();

                var firebaseAuth = FirebaseAuth.GetAuth(firebaseApp);

                if (context.Request.Headers.ContainsKey(TOKEN_HEADER_NAME))
                {
                    try
                    {
                        var idToken = context.Request.Headers[TOKEN_HEADER_NAME];
                        var token = await firebaseAuth.VerifyIdTokenAsync(idToken.First());

                        var user = await firebaseAuth.GetUserAsync(token.Uid);
                        context.Items.Add("CurrentUser", user);

                        await next.Invoke();
                    }
                    catch (FirebaseAuthException authException)
                    {
                        throw new UnauthorizedException(authException);
                    }
                }
                else
                {
                    throw new UnauthorizedException();
                }
            });


            app.UseMvc();
        }
    }
}
