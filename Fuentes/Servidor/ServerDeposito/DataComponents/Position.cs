﻿using System.ComponentModel.DataAnnotations;

namespace DACS.Distribuidora.DataComponents
{
    public class Position
    {
        [Key]
        public decimal Latitude { get; set; }

        public decimal Longitude { get; set; }
    }
}
