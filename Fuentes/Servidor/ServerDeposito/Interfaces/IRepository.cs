﻿using DACS.Distribuidora.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace DACS.Distribuidora.Interfaces
{
    public interface IRepository
    {
        Task<T> GetById<T>(int id) where T : BaseEntity;
        Task<List<T>> List<T>() where T : BaseEntity;
        IQueryable<T> GetAll<T>() where T : BaseEntity;
        Task<T> Add<T>(T entity) where T : BaseEntity;
        Task Update<T>(T entity) where T : BaseEntity;
        Task Delete<T>(T entity) where T : BaseEntity;
    }
}


