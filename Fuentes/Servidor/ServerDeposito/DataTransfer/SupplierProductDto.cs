﻿
namespace DACS.Distribuidora.DataTransfer
{
    public class SupplierProductDto
    {
        public virtual int Code { get; set; }
        public virtual int SupplierId { get; set; }
        public virtual int ProductId { get; set; }
        public virtual decimal CostPrice { get; set; }
        public virtual decimal ListPrice { get; set; }
    }
}
