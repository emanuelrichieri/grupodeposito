﻿using System.Collections.Generic;

namespace DACS.Distribuidora.DataTransfer
{
    public class ProductDto
    {
        public virtual string Code { get; set; }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual string Brand { get; set; }

        public virtual byte[] Image { get; set; }

        public virtual ICollection<SupplierProductDto> SupplierProducts { get; set; }

    }
}
