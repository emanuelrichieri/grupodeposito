﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.DataTransfer
{
    public class StockDto
    {
        public virtual int quantity { get; set; }
        public virtual int idProduct { get; set; }
        public virtual int idZone { get; set; }

    }
}
