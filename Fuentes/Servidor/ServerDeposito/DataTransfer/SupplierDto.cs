﻿using System.Collections.Generic;

namespace DACS.Distribuidora.DataTransfer
{
    public class SupplierDto
    {
        public virtual string Cuit { get; set; }
        public virtual string Name { get; set; }
        public virtual string Adress { get; set; }
        public virtual string Telephone { get; set; }
        public virtual string Email { get; set; }
        public virtual bool Active { get; set; }
        public virtual ICollection<SupplierProductDto> SupplierProducts { get; set; }
        public virtual ICollection<ReplenishmentRequestDto> ReplenishmentRequests { get; set; }

    }
}
