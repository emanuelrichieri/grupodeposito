﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.DataTransfer
{
    public class ProductZoneDto
    {
        public virtual ZoneDto Zone { get; set; }

        public virtual ProductDto Product { get; set; }

        public virtual decimal Quantity { get; set; }
    }
}
