﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.DataTransfer
{
    public class ReplenishmentLineDto
    {
        public virtual int IdProduct { get; set; }

        public virtual decimal Quantity { get; set; }
    }
}
