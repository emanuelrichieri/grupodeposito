﻿namespace DACS.Distribuidora.DataTransfer
{
    public class MinimalExistenceDto
    {
        public virtual decimal Quantity { get; set; }
        public virtual ProductDto Product { get; set; }
    }
}
