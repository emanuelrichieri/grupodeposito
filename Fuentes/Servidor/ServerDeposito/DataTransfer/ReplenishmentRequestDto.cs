﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.DataTransfer
{
    public class ReplenishmentRequestDto
    {
        public virtual DateTime Date { get; set; }
        public virtual int Status { get; set; }
        public virtual int SupplierId { get; set; }
        public virtual SupplierDto Supplier { get; set; }
        public virtual ICollection<ReplenishmentLineDto> Lines { get; set; }
       
    }
}
