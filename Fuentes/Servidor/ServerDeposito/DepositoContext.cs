﻿using Microsoft.EntityFrameworkCore;
using DACS.Distribuidora.Entities; 

namespace ServerDeposito
{
    public class DepositoContext : DbContext
    {
        public DepositoContext(DbContextOptions<DepositoContext> options)
            : base(options)
        {
        }

        public DbSet<MinimalExistence> MinimalExistences { get; set; }
        public DbSet<Product> Products { get; set; }

        public DbSet<ProductZone> ProductZones { get; set; }

        public DbSet<SupplierProduct> SupplierProducts { get; set; }

        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public DbSet<ReplenishmentLine> ReplenishmentLines { get; set; }
        public DbSet<ReplenishmentRequest> ReplenishmentRequests { get; set; }
        public DbSet<Zone> Zone { get; set; }


    }
}