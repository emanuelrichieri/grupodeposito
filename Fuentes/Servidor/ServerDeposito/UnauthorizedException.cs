﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora
{
    public class UnauthorizedException : Exception
    {
        public UnauthorizedException(Exception inner)
            : base("El request no contiene el token de identificacion del usuario o el token es invalido.", inner)
        {

        }

        public UnauthorizedException()
            : base("El request no contiene el token de identificacion del usuario o el token es invalido.")
        {

        }
    }
}