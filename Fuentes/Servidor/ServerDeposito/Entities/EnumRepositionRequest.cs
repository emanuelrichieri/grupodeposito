﻿namespace DACS.Distribuidora.Entities
{
    public enum EnumReplenishmentRequest
    {
        Pending,    //Pendiente
        Issued,     //Emitida
        Approved,   //Aprobada
        Rejected,   //Rechazada
        InProgress, //En progreso
        Voided,     //Anulada
    }
}
