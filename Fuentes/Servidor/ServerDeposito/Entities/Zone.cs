﻿using DACS.Distribuidora.DataComponents;

namespace DACS.Distribuidora.Entities
{
    public class Zone : BaseEntity
    {
        public virtual int StoreNumber { get; set; }

        public virtual int Corridor { get; set; }

        public virtual char Side { get; set; }

        public virtual int Cabinet { get; set; }

        public virtual Position Position { get; set; }
    }
}
