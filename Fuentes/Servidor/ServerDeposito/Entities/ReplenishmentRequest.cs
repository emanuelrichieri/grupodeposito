﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.Entities
{
    public class ReplenishmentRequest : BaseEntity
    {
        public virtual DateTime Date { get; set; }
        public virtual EnumReplenishmentRequest Status { get; set; }
        public virtual int SupplierId { get; set; }

        public virtual Supplier Supplier { get; set; }
        public virtual ICollection<ReplenishmentLine> Lines { get; set; }

    }
}
