﻿namespace DACS.Distribuidora.Entities
{
    public class MinimalExistence : BaseEntity
    {
        public virtual decimal Quantity { get; set; }
        public virtual Product Product { get; set; }

    }
}
