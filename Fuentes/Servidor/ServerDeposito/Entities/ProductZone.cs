﻿namespace DACS.Distribuidora.Entities
{
    public class ProductZone : BaseEntity
    {
        public virtual Zone Zone { get; set; }

        public virtual Product Product { get; set; }

        public virtual decimal Quantity { get; set; }
    }
}
