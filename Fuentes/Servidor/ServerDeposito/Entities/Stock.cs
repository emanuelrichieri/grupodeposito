﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.Entities
{
    public class Stock : BaseEntity
    {
        public virtual int quantity { get; set; }
        public virtual int idProduct { get; set; }
        public virtual int idZone { get; set; }
        public virtual Product Product { get; set; }
        public virtual Zone Zone { get; set; }
    }
}
