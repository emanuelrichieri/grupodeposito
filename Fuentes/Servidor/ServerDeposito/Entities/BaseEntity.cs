﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.Entities
{
    public abstract class BaseEntity
    {
        [Key]
        public virtual long Id { get; protected set; }
    }
}
