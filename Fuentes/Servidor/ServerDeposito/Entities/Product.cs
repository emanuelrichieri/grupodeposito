﻿using System.Collections.Generic;

namespace DACS.Distribuidora.Entities
{
    public class Product : BaseEntity
    {
        public virtual string Code { get; set; }

        public virtual string Name { get; set; }

        public virtual string Description { get; set; }

        public virtual string Brand { get; set; }

        public virtual byte[] Image { get; set; }

        public virtual ICollection<ProductZone> Stock { get; set; }
        public virtual ICollection<ReplenishmentLine> ReplenishmentLines { get; set; }
    }
}
