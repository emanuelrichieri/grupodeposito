﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.Entities
{
    public class SupplierProduct : BaseEntity
    {
        //public virtual int Id { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual Product Product { get; set; }
        public virtual decimal CostPrice { get; set; }
        public virtual decimal ListPrice { get; set; }
    }
}
