﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DACS.Distribuidora.Entities
{
    public class ReplenishmentLine : BaseEntity
    {
        public virtual int IdProduct { get; set; }
        public virtual decimal Quantity { get; set; }

    }
}
