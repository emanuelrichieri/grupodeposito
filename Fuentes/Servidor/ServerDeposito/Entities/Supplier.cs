﻿using DACS.Distribuidora.DataComponents;
using System.Collections.Generic;

namespace DACS.Distribuidora.Entities
{
    public class Supplier : BaseEntity
    {
        public virtual string Cuit { get; set; }
        public virtual string Name { get; set; }
        public virtual string Adress { get; set; }
        public virtual string Telephone { get; set; }
        public virtual string Email { get; set; }
        public virtual bool Active { get; set; }
        public virtual ICollection<SupplierProduct> SupplierProducts { get; set; }
        public virtual ICollection<ReplenishmentRequest> ReplenishmentRequests { get; set; }
    }
}
