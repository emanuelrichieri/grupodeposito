import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

import { AuthService } from './providers/auth.service';
import { User } from 'firebase/app';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  dark = false;
  user: User;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private afAuth: AngularFireAuth,
    private router: Router,
    private authService: AuthService,
    private menu: MenuController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    let dark = localStorage.getItem('dark');
    if (dark && dark == 'true') {
      this.dark = true;
    }
    this.platform.ready().then(() => {
      this.afAuth.user.subscribe(user => {
        if(user){
          this.user = user;
          this.router.navigate(["/home"]);
        } else {
          this.router.navigate(["/login"]);
        }
      }, 
      err => {
        this.router.navigate(["/login"]);
      }, 
      () => {
        this.splashScreen.hide();
      })
      this.statusBar.styleDefault();
    });
  }

  logout() {
    this.authService.doLogout().then(() => {
      this.user = null;
      this.router.navigate(["/login"]);
    });
  }

  toggleDarkMode() {
    localStorage.setItem('dark', this.dark ? 'true' : 'false');
  }
}
