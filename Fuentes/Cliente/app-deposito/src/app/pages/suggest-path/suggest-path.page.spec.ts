import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuggestPathPage } from './suggest-path.page';

describe('SuggestPathPage', () => {
  let component: SuggestPathPage;
  let fixture: ComponentFixture<SuggestPathPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuggestPathPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuggestPathPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
