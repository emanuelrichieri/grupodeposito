import { Component, OnInit } from '@angular/core';
import { Zone } from 'src/app/model/zone/zone';
import { ActivatedRoute } from '@angular/router';
import { ZoneService } from 'src/app/providers/zone.service';

@Component({
  selector: 'app-suggest-path',
  templateUrl: './suggest-path.page.html',
  styleUrls: ['./suggest-path.page.scss'],
})
export class SuggestPathPage implements OnInit {

  zones: Zone[];
  suggestedPathList: Zone[];

  constructor(private route: ActivatedRoute,
            private zoneService: ZoneService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      paramMap => {
        this.zoneService.getZones().subscribe(
          zones => {
            this.zones = zones.filter(zone => zone.getCriticalStock().length > 0);
          }
        )
      }
    );
  }

  suggestPath() {
    this.suggestedPathList = this.zoneService.suggestPath(this.zones);
  }

}
