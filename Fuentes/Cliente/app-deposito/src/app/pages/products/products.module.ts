import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProductsPage } from './products.page';
import { ProductsFilterPage } from './products-filter/products-filter.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { ProductSuppliersPage } from './product-suppliers/product-suppliers.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProductsPage, ProductsFilterPage, ProductSuppliersPage],
  entryComponents: [ProductsFilterPage, ProductSuppliersPage]
})
export class ProductsPageModule {}
