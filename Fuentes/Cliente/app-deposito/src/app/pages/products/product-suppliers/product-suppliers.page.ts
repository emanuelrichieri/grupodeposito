import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { SupplierService } from 'src/app/providers/supplier.service';
import { Product } from 'src/app/model/product/product';
import { Supplier } from 'src/app/model/supplier/supplier';
import { CommonService } from 'src/app/providers/common.service';
import { ProductService } from 'src/app/providers/product.service';

@Component({
  selector: 'app-product-suppliers',
  templateUrl: './product-suppliers.page.html',
  styleUrls: ['./product-suppliers.page.scss'],
})
export class ProductSuppliersPage implements OnInit {

  product: Product;
  suppliers: Supplier[];
  addedSupplier: {
    supplier: Supplier,
    costPrice: number,
    listPrice: number
  };

  interfaceOptions: any = {
    header: 'Proveedor'
  }

  constructor(
    private modalController: ModalController,
    private supplierService: SupplierService,
    private productService: ProductService,
    private navParams: NavParams,
    private common: CommonService) { }

  ngOnInit() {
    this.product = this.navParams.get("product");
    this.supplierService.getSuppliers().subscribe(suppliers => this.suppliers = suppliers);
  }

  remove(index: number) {
    this.product.suppliers.splice(index, 1);
  }

  add() {
    if (this.getSuppliersToAdd().length == 0) {
      this.common.presentToast('No hay más proveedores para agregar.', 'warning');
      return;
    }
    this.addedSupplier = {
      supplier: null,
      costPrice: 0,
      listPrice: 0
    }
  }

  addSupplierToList() {
    if (this.addedSupplier && this.addedSupplier.supplier) {
      if (this.addedSupplier.supplier) {
        this.product.suppliers.push(this.addedSupplier);
        this.addedSupplier = null;
      } else {
        this.common.presentToast('Seleccione un proveedor', 'warning');
      }
    } 
  }

  accept() {
    this.productService.saveSuppliers(this.product).subscribe(
      response => {
        let toastColor = 'success';
        let toastMessage = 'Proveedores asociados exitosamente.';
        if (response != null && response.status == false) {
          toastColor = 'danger';
          toastMessage = 'Ocurrió un error al asociar los proveedores.';
        } 
        this.common.presentToast(toastMessage, toastColor);
        this.dismiss({});
      }
    )
  }

  dismiss(data?: any) {
    this.modalController.dismiss(data);
  }

  getSuppliersToAdd(): Supplier[] {
    return this.suppliers.filter(supplier => 
      !this.product.suppliers.map(element => element.supplier.id).includes(supplier.id)
    );
  }

}
