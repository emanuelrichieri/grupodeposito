import { Component, OnInit } from '@angular/core';
import { ProductFilterData } from '../../../model/product/product-filter-data';
import { ModalController, NavParams } from '@ionic/angular';
import { SupplierService } from 'src/app/providers/supplier.service';
import { Supplier } from 'src/app/model/supplier/supplier';
import { Zone } from 'src/app/model/zone/zone';
import { ZoneService } from 'src/app/providers/zone.service';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-products-filter',
  templateUrl: './products-filter.page.html',
  styleUrls: ['./products-filter.page.scss'],
})
export class ProductsFilterPage implements OnInit {

  filterData: ProductFilterData;
  suppliers: Supplier[];
  zones: Zone[];

  constructor(
    private modalController: ModalController,
    private supplierService: SupplierService,
    private zoneService: ZoneService,
    private navParams: NavParams,
    private common: CommonService
  ) { }

  ngOnInit() {
    this.initFilterData();
    this.supplierService.getSuppliers().subscribe(suppliers => this.suppliers = suppliers);
    this.zoneService.getZones().subscribe(zones => this.zones = zones);
  }

  applyFilters() {
    this.dismiss(this.filterData);
  }

  dismiss(data?: any) {
    this.modalController.dismiss(data);
  }

  resetFilters() {
    this.filterData = {
      suppliers: [],
      zones: [],
      stockRange: {
        lower: 0,
        upper: 500
      },
      priceRange: {
        lower: 0,
        upper: 10000
      }
    }
  }

  initFilterData() {
    let suppliers = this.navParams.get("suppliers");
    let zones = this.navParams.get("zones");
    let stockRange = this.navParams.get("stockRange");
    let priceRange = this.navParams.get("priceRange");
    this.filterData = {
      suppliers: suppliers == undefined ? [] : suppliers,
      zones: zones == undefined ? [] : zones,
      stockRange: {
        lower: stockRange == undefined || stockRange.lower == undefined ? 0 : stockRange.lower,
        upper: stockRange == undefined || stockRange.upper == undefined ? 500 : stockRange.upper
      },
      priceRange: {
        lower: priceRange == undefined || priceRange.lower == undefined ? 0 : priceRange.lower,
        upper: priceRange == undefined || priceRange.upper == undefined ? 10000 : priceRange.upper,
      }
    }
  }

  compareWith = this.common.compareWithFn;
}
