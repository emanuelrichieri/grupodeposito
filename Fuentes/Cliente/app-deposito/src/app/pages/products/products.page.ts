import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../providers/product.service'
import { Router } from '@angular/router';
import { Product } from '../../model/product/product';
import { ProductFilterData } from '../../model/product/product-filter-data';
import { ModalController } from '@ionic/angular';
import { ProductsFilterPage } from './products-filter/products-filter.page';
import { CommonService } from 'src/app/providers/common.service';
import { ProductSuppliersPage } from './product-suppliers/product-suppliers.page';

@Component({
  selector: 'app-products',
  templateUrl: './products.page.html',
  styleUrls: ['./products.page.scss'],
})
export class ProductsPage implements OnInit {

  products: Product[] = [];
  filterData: ProductFilterData;

  showSearchBar = false;
  searchInput = '';

  constructor(private productService: ProductService,
              private router: Router,
              private modalController: ModalController,
              private common: CommonService) { }

  ngOnInit() {
    this.productService.getProducts().subscribe(
      result => {
        this.products = result; 
        this.filterProducts();
    });
  }

  async setFilters() {
    const modal = await this.modalController.create({
      component: ProductsFilterPage,
      componentProps: this.filterData
    });
    await modal.present();

    const onWillDismiss = await modal.onWillDismiss();
    if (onWillDismiss) {
      this.filterData = onWillDismiss.data;
      this.filterProducts();
    }
  }

  filterProducts() {
    this.products = this.productService.filterProducts(this.filterData);
  }

  onCancelSearch() {
    this.showSearchBar = false;
    this.searchInput = '';
  }

  select(product) {
    this.products.map((element: any) => element.id == product.id ? product.selected = !product.selected : element.selected = false);
  }

  async associateToSupplier() {
    if (this.getSelectedItem()) {
      const modal = await this.modalController.create({
        component: ProductSuppliersPage,
        componentProps: {
          product: this.getSelectedItem()
        }
      });
      await modal.present();

      const onWillDismiss = await modal.onWillDismiss();
      if (onWillDismiss) {
        this.filterProducts();
      }
    } else {
      this.common.presentToast('Seleccione un producto.', 'warning');
    }
  }

  getSelectedItem() {
    return this.products.find((product: any) => product.selected); 
  }

}
