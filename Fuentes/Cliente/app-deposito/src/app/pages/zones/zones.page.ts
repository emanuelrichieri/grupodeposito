import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Zone} from '../../model/zone/zone';
import {ZoneService} from '../../providers/zone.service';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-zones',
  templateUrl: './zones.page.html',
  styleUrls: ['./zones.page.scss'],
})
export class ZonesPage implements OnInit {
  zones: Zone[]=[];
  showSearchBar = false;
  searchInput = '';

  constructor(
    private zoneService: ZoneService,
    private router: Router,
    private common: CommonService) { }

  ngOnInit() {
    this.router.events.subscribe(
      _ => {
        this.zoneService.getZones().subscribe(
          result => {
            this.zones = result;
        });
      }
    )    
  }

  onCancelSearch() {
    this.showSearchBar = false;
    this.searchInput = '';
  }


  select(zone) {
    this.zones.map((element: any) => element.id == zone.id ? zone.selected = !zone.selected : element.selected = false);
  }

  getSelectedItem(): Zone {
    return this.zones.find((zone: any) => zone.selected);
  }

  updateStock() {
    let zone = this.getSelectedItem();
    if (zone) {
      this.router.navigate([`/stock/${zone.id}`]);
    } else {
      this.common.presentToast('Seleccione una zona.', 'warning');
    }    
  }

}
