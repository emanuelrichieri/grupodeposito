import { Component, OnInit } from '@angular/core';
import { Supplier } from '../../../model/supplier/supplier';
import { SupplierService } from 'src/app/providers/supplier.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { CommonService } from 'src/app/providers/common.service';
import { Product } from 'src/app/model/product/product';
import { ProductService } from 'src/app/providers/product.service';

@Component({
  selector: 'app-supplier-detail',
  templateUrl: './supplier-detail.page.html',
  styleUrls: ['./supplier-detail.page.scss'],
})
export class SupplierDetailPage implements OnInit {
  supplier: Supplier = new Supplier();
  supplierForm: FormGroup;
  products: Product[] = [];
  addedProduct: {
    product: Product,
    costPrice: number,
    listPrice: number
  };

  interfaceOptions: any = {
    header: 'Producto'
  }

  constructor(private supplierService: SupplierService, 
              private formBuilder: FormBuilder,
              private route: ActivatedRoute,
              private location: Location,
              private common: CommonService,
              private router: Router,
              private productService: ProductService) 
  {
    this.supplierForm = this.formBuilder.group({
      cuit: ["", Validators.required],
      name: ["", Validators.required],
      phone:["", Validators.required],
      address:["", Validators.required],
      email:["", Validators.required],
      active:[true]
    }); 
  }

  ngOnInit() {   
    let supplierId = this.route.snapshot.paramMap.get('id');
    this.supplierService.getSupplier(supplierId).subscribe(
      supplier => {
        this.supplier = supplier;
        if (this.supplier == null) {
          this.supplier = new Supplier();
          this.common.presentToast('Ocurrió un error al obtener el proveedor', 'danger');
        }
      }
    );
    this.productService.getProducts().subscribe(products => this.products = products);
  }

  onAccept() {
    if (this.supplierForm.valid) {
      this.supplierService.saveSupplier(this.supplier).subscribe(
        response => {
          let toastColor = 'success';
          let toastMessage = 'Proveedor guardado exitosamente.';
          if (response != null && response.status == false) {
            toastColor = 'danger';
            toastMessage = 'Ocurrió un error al guardar el proveedor.';
          } 
          this.common.presentToast(toastMessage, toastColor);
          this.router.navigate(['/suppliers']);        
        }
      ); 
    } else {
      this.common.presentToast('Complete los campos faltantes. ', 'warning');
    }
  }
  onCancel() {
    this.location.back();  
  }

  removeProduct(index: number) {
    this.supplier.products.splice(index, 1);
  }

  addProduct() {
    if (this.getProductsToAdd().length == 0) {
      this.common.presentToast('No hay más productos para agregar.', 'warning');
      return;
    }
    this.addedProduct = {
      product: null,
      costPrice: 0,
      listPrice: 0
    }
  }
  
  addProductToList() {
    if (this.addedProduct && this.addedProduct.product) {
      if (this.addedProduct.product) {
        this.supplier.products.push(this.addedProduct);
        this.addedProduct = null;
      } else {
        this.common.presentToast('Seleccione un producto', 'warning');
      }
    } 
  }

  getProductsToAdd(): Product[] {
    return this.products.filter(product => 
      !this.supplier.products.map(element => element.product.id).includes(product.id)
    );
  }

}
