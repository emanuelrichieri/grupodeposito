import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SuppliersPage } from './suppliers.page';
import { SuppliersFiltersPage } from './suppliers-filters/suppliers-filters.page';
import { SupplierDetailPage } from './supplier-detail/supplier-detail.page';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { NgxMaskIonicModule } from 'ngx-mask-ionic';

const routes: Routes = [
  { 
    path: '', 
    children: [
      { path: '', component: SuppliersPage},
      { path: 'supplier', component: SupplierDetailPage },
      { path: 'supplier/:id', component: SupplierDetailPage }
    ]
  }
  
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PipesModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    NgxMaskIonicModule
  ],
  declarations: [
    SuppliersPage, 
    SuppliersFiltersPage, 
    SupplierDetailPage
  ],
  entryComponents: [
    SuppliersFiltersPage
  ]
})
export class SuppliersPageModule {}
