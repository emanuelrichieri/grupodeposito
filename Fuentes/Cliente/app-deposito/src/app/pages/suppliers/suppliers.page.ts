import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SuppliersFiltersPage } from './suppliers-filters/suppliers-filters.page';
import { Supplier } from 'src/app/model/supplier/supplier';
import { SupplierService } from 'src/app/providers/supplier.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.page.html',
  styleUrls: ['./suppliers.page.scss'],
})
export class SuppliersPage implements OnInit {

  suppliers: Supplier[] = [];
  showSearchBar = false;
  searchInput = '';
  filterData: { status: string } = { status: 'active' };

  private _supplierDetailPage = '/suppliers/supplier';

  constructor(
    private supplierService: SupplierService,
    private modalController: ModalController,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(
      _ => {
        this.supplierService.getSuppliers().subscribe(
          result => {
            this.suppliers = result; 
            this.filterSuppliers();
        });
      }
    )
  }

  async setFilters() {
    const modal = await this.modalController.create({
      component: SuppliersFiltersPage,
      componentProps: this.filterData
    });
    await modal.present();

    const onWillDismiss = await modal.onWillDismiss();
    if (onWillDismiss) {
      this.filterData = onWillDismiss.data;
      this.filterSuppliers();
    }
  }

  filterSuppliers() {
    this.suppliers = this.supplierService.filterSuppliers(this.filterData);
  }

  addSupplier() {
    this.router.navigate([this._supplierDetailPage]);
  }

  editSupplier(supplier: Supplier) {
    this.router.navigate([`${this._supplierDetailPage}/${supplier.id}`]);
    
  }

  onCancelSearch() {
    this.showSearchBar = false;
    this.searchInput = '';
  }

}
