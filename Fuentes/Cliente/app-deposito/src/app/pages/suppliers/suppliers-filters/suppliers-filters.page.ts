import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';

@Component({
  selector: 'app-suppliers-filters',
  templateUrl: './suppliers-filters.page.html',
  styleUrls: ['./suppliers-filters.page.scss'],
})
export class SuppliersFiltersPage implements OnInit {

  statusSelect: string;
  filterData: NavParams;

  constructor(
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.filterData = this.navParams;
    this.statusSelect = this.filterData.get('status');
    if (this.statusSelect === undefined || this.statusSelect === null) {
      this.statusSelect = 'active';
    }
  }

  applyFilters() {
    let data = {
      status: this.statusSelect
    };
    this.dismiss(data);
  }

  dismiss(data?: any) {
    this.modalController.dismiss(data);
  }

  resetFilters() {
    this.statusSelect = 'active';
  }

}
