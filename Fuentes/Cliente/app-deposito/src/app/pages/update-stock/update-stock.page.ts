import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../providers/product.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Zone } from '../../model/zone/zone';
import { Product } from '../../model/product/product';
import { ZoneService } from '../../providers/zone.service';
import { StockService } from '../../providers/stock.service';
import { CommonService } from '../../providers/common.service';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

@Component({
  selector: 'app-update-stock',
  templateUrl: './update-stock.page.html',
  styleUrls: ['./update-stock.page.scss'],
})
export class UpdateStockPage implements OnInit {

  zone: Zone;
  defaultZone: Zone;
  product: Product;
  quantity: number;
  stock: {
    idProduct: number,
    idZone: number,
    quantity: number
  }[] = [];
  products: Product[];
  zones: Zone[];

  constructor(
    private productService: ProductService,
    private zoneService: ZoneService,
    private route: ActivatedRoute,
    private router: Router,
    private common: CommonService,
    private stockService: StockService,
    private barcodeScanner: BarcodeScanner,
  ) {   }

  ngOnInit() {
    this.route.paramMap.subscribe(
      paramMap => {
        let zoneId = paramMap.get('idZone');
        if (zoneId) {
          this.zoneService.getZone(zoneId).subscribe(zone => this.defaultZone = zone);
        }
        this.productService.getProducts().subscribe(products => this.products = products); 
        this.zoneService.getZones().subscribe(zones => {
          this.zones = zones;
          this.findZone();
        });
      }
    )
  }

  /***
   * Find zone.
   *  Default zone if it was setted. Zone located using maps otherwise.
   */
  findZone() {
    if (this.defaultZone) {
      this.zone = this.defaultZone;
      return;
    }
    let zonesOrderedByDistance = this.zoneService.getSortedByDistance(this.zones);
    if (zonesOrderedByDistance.length > 0) {
      this.zone = zonesOrderedByDistance[0];
      return;
    }
    if (this.zones.length > 0) {
      this.zone = this.zones[0];
    }
  }

  /***
   * Refresh stock value according to selected product and zone. 
   */
  refreshStock() {
    this.findZone();
    if (this.zone) {      
      this.quantity = 0;
      let stockElement = this.stock.find(element => element.idProduct == this.product.id && element.idZone == this.zone.id);
      if (stockElement) {
        this.quantity = stockElement.quantity;
      } else {
        let productStock = this.product.stock.find(element => element.zone.id == this.zone.id);
        if (productStock) {
          this.quantity = productStock.quantity;
        }
      }
    } 
  }
 
  scanProduct() {
    this.barcodeScanner.scan().then(barcodeData => {
      if (!barcodeData.cancelled) {
        let product = this.products.find(product => product.code == barcodeData.text);
        if (product) {
          this.product = product;
          this.refreshStock();
        } else {
          this.common.presentToast('No existe producto con el código de barras ingresado.', 'danger');
        }
      }
     }).catch(err => {
         this.common.presentToast(err, 'danger');
     });
  }

  saveAndContinue() {
    let stockElement = this.stock.find(element => element.idProduct == this.product.id && element.idZone == this.zone.id);
    if (stockElement) {
      stockElement.quantity = this.quantity;
    } else {
      stockElement = {
        idProduct: this.product.id,
        idZone: this.zone.id,
        quantity: this.quantity
      }
    }
    this.stock.push(stockElement);
    
    this.zone = null;
    this.product = null;
    this.quantity = null;  
  }

  onCancel() {
    this.defaultZone = null;
    this.router.navigate(['products']);
  }

  onAccept() {
    this.stockService.updateStock(this.stock).subscribe(
      response => {
        let toastColor = 'success';
        let toastMessage = 'Los datos fueron cargados exitosamente.';
        if (response != null && response.status == false) {
          toastColor = 'danger';
          toastMessage = 'Ocurrió un error al registrar las existencias.';
        } 
        this.common.presentToast(toastMessage, toastColor);
        this.router.navigate(['products']);
      }
    )
  }

  compareWith = this.common.compareWithFn;
}
