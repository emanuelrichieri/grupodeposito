import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UpdateStockPage } from './update-stock.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateStockPage
  },
  {
    path: ':idZone',
    component: UpdateStockPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [UpdateStockPage]
})
export class UpdateStockPageModule {}
