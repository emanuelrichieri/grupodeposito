import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Supplier } from 'src/app/model/supplier/supplier';
import { Product } from 'src/app/model/product/product';
import { ReplenishmentFilterData } from 'src/app/model/replenishment/replenishment-filter-data';
import { SupplierService } from 'src/app/providers/supplier.service';
import { ProductService } from 'src/app/providers/product.service';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-replenishments-filters',
  templateUrl: './replenishments-filters.page.html',
  styleUrls: ['./replenishments-filters.page.scss'],
})
export class ReplenishmentsFiltersPage implements OnInit {
  filterData: ReplenishmentFilterData;
  suppliers: Supplier[];
  products: Product[];

  constructor(
    private supplierService: SupplierService,
    private productService: ProductService,
    private common: CommonService,
    private navParams: NavParams,
    private modalController: ModalController
  ) { }

  ngOnInit() {
    this.initFilterData();
    this.supplierService.getSuppliers().subscribe(
      suppliers => this.suppliers = suppliers
    );
    this.productService.getProducts().subscribe(
      products => this.products = products
    );
  }

  applyFilters() {
    this.dismiss(this.filterData);
  }

  dismiss(data?: any) {
    this.modalController.dismiss(data);
  }

  resetFilters() {
    this.filterData = {
      status: [],
      suppliers: [],
      products: [],
      dateFrom: null,
      dateTo: null
    }
  }

  initFilterData() {
    let status = this.navParams.get("status");
    let suppliers = this.navParams.get("suppliers");
    let products = this.navParams.get("products");
    let dateFrom = this.navParams.get("dateFrom");
    let dateTo = this.navParams.get("dateTo");
    this.filterData = {
      status: status == undefined ? '' : status,
      suppliers: suppliers == undefined ? [] : suppliers,
      products: products == undefined ? [] : products,
      dateFrom: dateFrom == undefined  ? null : dateFrom,
      dateTo: dateTo == undefined ? null : dateTo
    }
  }

  compareWith = this.common.compareWithFn;
}
