import { Component, OnInit} from '@angular/core';
import { Replenishment } from '../../../model/replenishment/replenishment';
import { ReplenishmentService, ReplenishmentStatus } from 'src/app/providers/replenishment.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Supplier } from '../../../model/supplier/supplier';
import { SupplierService } from '../../../providers/supplier.service'
import { Product } from 'src/app/model/product/product';
import { ProductService } from 'src/app/providers/product.service';
import { CommonService } from 'src/app/providers/common.service';

@Component({
  selector: 'app-replenishment-detail',
  templateUrl: './replenishment-detail.page.html',
  styleUrls: ['./replenishment-detail.page.scss'],
})
export class ReplenishmentDetailPage implements OnInit {

  replenishment: Replenishment = new Replenishment();
  canBeModified: boolean;
  replenishmentForm: FormGroup;
  validStatus: string[] = [ReplenishmentStatus.PENDING, ReplenishmentStatus.ISSUED];
  suppliers: Supplier[];
  filterData: { status: string } = { status: 'active' };
  products: Product[] = [];
  addedProduct: {
    product: Product,
    quantity: number
  };
  replenishmentDate: string;
  

  constructor(private replenishmentService: ReplenishmentService, 
    private supplierService: SupplierService,
    private productService: ProductService,
    private common: CommonService, 
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private location: Location,
    private router: Router)
     {
       let currentDate = new Date();
      this.replenishmentForm = this.formBuilder.group({
        number: [ { value: null, disabled: true }],
        date: [ currentDate.toISOString(), Validators.required],
        supplier:[{ value: ""},Validators.required],
        status:["", Validators.required]
      });
    }

  ngOnInit() {    
    this.supplierService.getSuppliers().subscribe(
      result => {
        this.suppliers = result;
        this.suppliers = this.supplierService.filterSuppliers(this.filterData);
    });
    let replenishmentId = this.route.snapshot.paramMap.get('id');
    this.replenishmentService.getReplenishment(replenishmentId).subscribe(
      replenishment => {
        this.replenishment = replenishment;  
        this.canBeModified = this.replenishment.status == ReplenishmentStatus.PENDING || this.replenishment.status == '';
        this.validStatus = this.replenishmentService.getValidStatus(this.replenishment);
        if (replenishment == null) {
          this.replenishment = new Replenishment();
          this.validStatus=this.replenishmentService.getValidStatus(this.replenishment);
          this.common.presentToast('Ocurrió un error al obtener la solicitud', 'danger');
        }
        this.replenishmentDate = replenishment.date.toISOString();
      });
      this.productService.getProducts().subscribe(products => this.products = products);
    }

  onAccept() {
      if (this.replenishmentForm.valid) {
        this.replenishmentService.saveReplenishment(this.replenishment).subscribe(
          response => {
            let toastColor = 'success';
            let toastMessage = 'Solicitud guardada exitosamente.';
            if (response != null && response.status == false) {
              toastColor = 'danger';
              toastMessage = 'Ocurrió un error al guardar la solicitud.';
            } 
            this.common.presentToast(toastMessage, toastColor);
            this.router.navigate(['/replenishments']);        
          }
        ); 
      } else {
        this.common.presentToast('Complete los campos faltantes. ', 'warning');
      }
    }
  
  onCancel() {
    this.location.back();  
  }

  addProduct() {
    if (this.getProductsToAdd().length == 0) {
      this.common.presentToast('No hay más productos para agregar.', 'warning');
      return;
    }
    this.addedProduct = {
      product: null,
      quantity:0
    }
  }

  removeProduct(index: number) {
    this.replenishment.lines.splice(index, 1);
  }
  
  addProductToList() {
    if (this.addedProduct && this.addedProduct.product) {
      if (this.addedProduct.product) {
        this.replenishment.lines.push(this.addedProduct);
        this.addedProduct = null;
      } else {
        this.common.presentToast('Seleccione un producto', 'warning');
      }
    } 
  }

  getProductsToAdd(): Product[] {
    return this.products.filter(product => 
      !this.replenishment.lines.map(element => element.product.id).includes(product.id)
    );
  }

  setDate() {
    this.replenishment.date = new Date(Date.parse(this.replenishmentDate));
  }

  compareWith = this.common.compareWithFn;
}
