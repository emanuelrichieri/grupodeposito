import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ReplenishmentsFiltersPage } from './replenishments-filters/replenishments-filters.page';
import { Replenishment } from 'src/app/model/replenishment/replenishment';
import { ReplenishmentService } from 'src/app/providers/replenishment.service';
import { Router } from '@angular/router';
import { ReplenishmentFilterData } from 'src/app/model/replenishment/replenishment-filter-data';


@Component({
  selector: 'app-replenishments',
  templateUrl: './replenishments.page.html',
  styleUrls: ['./replenishments.page.scss'],
})
export class ReplenishmentsPage implements OnInit {

  replenishments: Replenishment[] = [];
  showSearchBar = false;
  searchInput = '';
  filterData: ReplenishmentFilterData;

  private _replenishmentDetailPage = '/replenishments/replenishment';

  constructor(private replenishmentService: ReplenishmentService,
    private modalController: ModalController,
    private router: Router) { }

  ngOnInit() {
    this.router.events.subscribe(
      _ => {
        this.replenishmentService.getReplenishments().subscribe(
          result => {
            this.replenishments = result;
        });
      }
    )   
  }

  async setFilters() {
    const modal = await this.modalController.create({
      component: ReplenishmentsFiltersPage,
      componentProps: this.filterData
    });
    await modal.present();

    const onWillDismiss = await modal.onWillDismiss();
    if (onWillDismiss) {
      this.filterData = onWillDismiss.data;
      this.filterReplenishments();
    }
  }

  filterReplenishments() {
    this.replenishments = this.replenishmentService.filterReplenishments(this.filterData);
  }

  addReplenishment() {
    this.router.navigate([this._replenishmentDetailPage]);
  }

  editReplenishment(replenishment: Replenishment) {
    this.router.navigate([`${this._replenishmentDetailPage}/${replenishment.id}`])
  }

  onCancelSearch() {
    this.showSearchBar = false;
    this.searchInput = '';
  }
}
