import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';
import { PipesModule } from '../../pipes/pipes.module';
import { ReplenishmentsPage } from './replenishments.page';
import { ReplenishmentDetailPage } from './replenishment-detail/replenishment-detail.page';
import { ReplenishmentsFiltersPage } from './replenishments-filters/replenishments-filters.page';

const routes: Routes = [
  {
    path: '',
    component: ReplenishmentsPage
  },
  {
    path: 'replenishment',
    component: ReplenishmentDetailPage
  },
  {
    path: 'replenishment/:id',
    component: ReplenishmentDetailPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PipesModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReplenishmentsPage, ReplenishmentDetailPage, ReplenishmentsFiltersPage],
  entryComponents: [ReplenishmentsFiltersPage]
})
export class ReplenishmentsPageModule {}
