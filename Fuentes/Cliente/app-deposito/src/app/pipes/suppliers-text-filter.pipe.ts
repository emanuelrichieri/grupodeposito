import { Pipe, PipeTransform } from '@angular/core';
import { Supplier } from '../model/supplier/supplier';

@Pipe({
  name: 'suppliersTextFilter'
})
export class SuppliersTextFilterPipe implements PipeTransform {

  /**
   * Text filter for Suppliers
   * @param suppliers suppliers array
   * @param text text filter
   */
  transform(suppliers: Supplier[], text: string): Supplier[] {
    if (text.length === 0) {
      return suppliers;
    }
    text = text.toLocaleLowerCase();
    return suppliers.filter(
      supplier =>
          supplier.name.toLocaleLowerCase().includes(text)
          || supplier.email.toLocaleLowerCase().includes(text)
          || supplier.phone.toLocaleLowerCase().includes(text)
          || supplier.address.toLocaleLowerCase().includes(text)
          || supplier.cuit.toLocaleLowerCase().includes(text)
    );
  }
}
