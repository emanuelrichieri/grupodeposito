import { NgModule } from '@angular/core';
import { SuppliersTextFilterPipe } from './suppliers-text-filter.pipe';
import { ZonesTextFilterPipe } from './zones-text-filter.pipe';
import { ProductsTextFilterPipe } from './products-text-filter.pipe';
import { ReplenishmentsTextFilter } from './replenishments-text-filter.pipe';

@NgModule({
  declarations: [SuppliersTextFilterPipe, ProductsTextFilterPipe, ZonesTextFilterPipe, ReplenishmentsTextFilter],
  imports: [],
  exports: [SuppliersTextFilterPipe, ProductsTextFilterPipe, ZonesTextFilterPipe, ReplenishmentsTextFilter]
})
export class PipesModule { }
