import { Pipe, PipeTransform } from '@angular/core';
import { Replenishment } from '../model/replenishment/replenishment';

@Pipe({
  name: 'replenishmentsTextFilter'
})
export class ReplenishmentsTextFilter implements PipeTransform {

  /**
   * Text filter for Replenishments
   * @param replenishments replenishments array
   * @param text text filter
   */
  transform(replenishments: Replenishment[], text: string): Replenishment[] {
    if (text.length === 0) {
      return replenishments;
    }
    text = text.toLocaleLowerCase();
    return replenishments.filter(
      replenishment =>
          replenishment.id.toLocaleString().includes(text)
          || replenishment.supplier.name.toLocaleLowerCase().includes(text)
          || replenishment.lines.filter(
                line => line.product.code.includes(text) || line.product.name.toLocaleLowerCase().includes(text)
            ).length > 0
    );
  }
}
