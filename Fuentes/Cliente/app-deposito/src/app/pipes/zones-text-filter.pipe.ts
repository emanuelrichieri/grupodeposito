import { Pipe, PipeTransform } from '@angular/core';
import { Zone } from '../model/zone/zone';

@Pipe({
  name: 'zonesTextFilter'
})
export class ZonesTextFilterPipe implements PipeTransform {

  /**
   * Text filter for Suppliers
   * @param zones zones array
   * @param text text filter
   */
  transform(zones: Zone[], text: string): Zone[] {
    if (text.length === 0) {
      return zones;
    }
    text = text.toLocaleLowerCase();
    return zones.filter(
      zone =>
          zone.id.toLocaleString().includes(text)
          || zone.storeNumber.toLocaleString().includes(text)
          || zone.getDescription().toLocaleLowerCase().includes(text)
    );
  }
}
