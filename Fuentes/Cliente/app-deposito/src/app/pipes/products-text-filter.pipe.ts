import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../model/product/product';

@Pipe({
  name: 'productsTextFilter'
})
export class ProductsTextFilterPipe implements PipeTransform {

  /**
   * Text filter for Products
   * @param products products array
   * @param text text filter
   */
  transform(products: Product[], text: string): Product[] {
    if (text.length === 0) {
      return products;
    }
    text = text.toLocaleLowerCase();
    return products.filter(
      product =>
          product.name.toLocaleLowerCase().includes(text)
          || product.description.toLocaleLowerCase().includes(text)
          || product.code.toLocaleLowerCase().includes(text)
          || product.brand.toLocaleLowerCase().includes(text)
    );
  }
}
