import { Injectable } from '@angular/core';
import * as salesman from 'src/assets/modules/salesman/salesman.js';
import { Zone } from '../model/zone/zone';

interface Position {
  latitude: number,
  longitude: number   
}

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  salesman;

  constructor() {
    this.salesman = salesman;
  }

  /***
   * Calculate distance in km
   * between two coords.
   */
  calculateDistance(coordsA: Position, coordsB: Position){
    let lat1 = coordsA.latitude;
    let lat2 = coordsB.latitude;
    let lon1 = coordsA.longitude;
    let lon2 = coordsB.longitude;

    let p = 0.017453292519943295;
    let c = Math.cos;
    let a = 0.5 - c((lat1-lat2) * p) / 2 + c(lat2 * p) *c((lat1) * p) * (1 - c(((lon1- lon2) * p))) / 2;
    let distance = (12742 * Math.asin(Math.sqrt(a)));
    return distance;
  }



  calculateRoute(zones: Zone[], startZone: Zone): Zone[] {
      let solution: [] = this.salesman.solve(
          zones.map(zone => new this.salesman.Point(zone.position.latitude, zone.position.longitude))
      );
      let route = solution.map(i => zones[i]);   
      let startIndex = route.findIndex(zone => zone.id == startZone.id);
      let result: Zone[] = [];
      for(let idx = startIndex; idx < route.length; idx++) {
        result.push(route[idx]);
      }
      for(let idx = 0; idx < startIndex; idx++) {
        result.push(route[idx]);
      }
      return result;
  }



}
