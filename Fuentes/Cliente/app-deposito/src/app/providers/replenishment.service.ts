import { Injectable } from '@angular/core';
import { Replenishment } from '../model/replenishment/replenishment';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import {ReplenishmentAdapter} from '../model/replenishment/replenishment-adapter';
import {ReplenishmentDTO} from '../model/replenishment/replenishmentDTO'
import { ReplenishmentFilterData } from '../model/replenishment/replenishment-filter-data';

export enum ReplenishmentStatus {
  PENDING = "PENDIENTE",
  ISSUED = "EMITIDA",
  APPROVED = "APROBADA",
  OVERRIDEN = "ANULADA",
  REJECTED = "RECHAZADA",
  IN_PROGRESS = "EN CURSO",
  FULFILLED = "CUMPLIDA"
}

@Injectable({
  providedIn: 'root'
})
export class ReplenishmentService {

  private _replenishmentsUrl = `${environment.apiUrl}/replenishmentRequests`;
  private _fetchedReplenishments: Replenishment[] = [];

  constructor(private http: HttpClient,
    private replenishmentAdapter: ReplenishmentAdapter ) { }

  getReplenishments(): Observable<Replenishment[]> {
    return this.http.get<Replenishment[]>(this._replenishmentsUrl).pipe(
      map((data: any[]) => {
        this._fetchedReplenishments = data.map(item => this.replenishmentAdapter.adapt(item));
        return this._fetchedReplenishments;
      }),
      catchError(error => {
        // TODO : log error
        this._fetchedReplenishments = [];
        return of(this._fetchedReplenishments);
      })
    );
  }


  /**
   * Apply filterData to fetchedReplenishments
   * @param filterData 
   * @returns products list with replenishment that match the given filters
   */

  filterReplenishments(filterData: ReplenishmentFilterData): Replenishment[] {
    return this._fetchedReplenishments.filter(
      replenishment => this.filterReplenishment(replenishment, filterData)
    );
  }
  
  
  /**
   * Determine if replenishment matches the given filter
   * @param replenishment 
   * @param filter 
   * @returns true, if it matches; false otherwise
   */
  private filterReplenishment(replenishment: Replenishment, filter: ReplenishmentFilterData): boolean {
    if (filter == undefined || filter == null) {
      return true;
    }

    let statusFilter = true;
    if (filter.status.length > 0){
      statusFilter = filter.status.includes(replenishment.status);
    }
    
    let productFilter = true;
    if (filter.products.length > 0) {
      productFilter = replenishment.lines.filter(
        line => filter.products.map(product => product.id).includes(line.product.id)
      ).length > 0;
    }
    
    let suppliersFilter = true;
    if (filter.suppliers.length > 0) {
      suppliersFilter = filter.suppliers.map(supplier => supplier.id).includes(replenishment.supplier.id)
    }
    
    let dateFromFilter = true;
    if (filter.dateFrom) {
      let dateFrom = new Date(filter.dateFrom);
      dateFrom.setHours(0, 0, 0, 0);
      dateFromFilter = replenishment.date.getTime() >= dateFrom.getTime();
    }

    let dateToFilter = true;
    if (filter.dateTo) {
      let dateTo = new Date(filter.dateTo);
      dateTo.setHours(0, 0, 0, 0);      
      dateToFilter = replenishment.date.getTime() <= dateTo.getTime();
    }

    return statusFilter && productFilter && suppliersFilter && dateFromFilter && dateToFilter;
  }

  saveReplenishment(replenishment: Replenishment): Observable<any> {
    if (replenishment.id == null) {
      return this.addReplenishment(replenishment);
    } else {
      return this.updateReplenishment(replenishment);
    }
  }

  addReplenishment(replenishment: Replenishment): Observable<any> {
    let replenishmentDTO: ReplenishmentDTO = this.replenishmentAdapter.transform(replenishment);
    return this.http.post(this._replenishmentsUrl, replenishmentDTO).pipe(
      catchError( error => { return of({status: false, error: error}) })
    );
  }

  updateReplenishment(replenishment: Replenishment): Observable<any> {
    let replenishmentDTO: ReplenishmentDTO = this.replenishmentAdapter.transform(replenishment);
    return this.http.put(`${this._replenishmentsUrl}/${replenishment.id}`, replenishmentDTO).pipe(
      catchError( error => { return of({status: false, error: error}) })
    );
  }

  getReplenishment(replenishmentId: string): Observable<Replenishment> {
    if (replenishmentId == null) {
      return of(new Replenishment());
    }
    return this.http.get<Replenishment>(`${this._replenishmentsUrl}/${replenishmentId}`).pipe(
      map( (replenishmentDTO: any) => this.replenishmentAdapter.adapt(replenishmentDTO)),
      catchError( error => { return of(null) })
    );
  }

  getValidStatus(replenishment: Replenishment): string[]{
    if (!replenishment.status || replenishment.status=="") {
       return [ReplenishmentStatus.PENDING];
    }
    if (replenishment.status == ReplenishmentStatus.PENDING) {
      return [ReplenishmentStatus.PENDING, ReplenishmentStatus.ISSUED, ReplenishmentStatus.OVERRIDEN];
    }
    if (replenishment.status == ReplenishmentStatus.ISSUED) {
      return [ReplenishmentStatus.ISSUED, ReplenishmentStatus.APPROVED, ReplenishmentStatus.REJECTED, ReplenishmentStatus.OVERRIDEN];
    }
    if (replenishment.status == ReplenishmentStatus.APPROVED) {
      return [ReplenishmentStatus.APPROVED, ReplenishmentStatus.IN_PROGRESS, ReplenishmentStatus.OVERRIDEN];
    }
    if (replenishment.status == ReplenishmentStatus.REJECTED) {
      return [ReplenishmentStatus.REJECTED, ReplenishmentStatus.PENDING, ReplenishmentStatus.ISSUED, ReplenishmentStatus.OVERRIDEN];
    }
    if (replenishment.status == ReplenishmentStatus.IN_PROGRESS) {
      return [ReplenishmentStatus.IN_PROGRESS, ReplenishmentStatus.FULFILLED, ReplenishmentStatus.OVERRIDEN];
    }
    if (replenishment.status == ReplenishmentStatus.FULFILLED) {
      return [ReplenishmentStatus.FULFILLED];
    } 
    if (replenishment.status == ReplenishmentStatus.OVERRIDEN) {
      return [ReplenishmentStatus.OVERRIDEN];
    }
    return [replenishment.status];
  }
}
