import { Injectable } from '@angular/core';
import { Product } from '../model/product/product';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ProductAdapter } from '../model/product/product-adapter';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ProductFilterData } from '../model/product/product-filter-data';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private _productsUrl = `${environment.apiUrl}/products`;
  private _fetchedProducts: Product[] = [];

  constructor(
    private http: HttpClient,
    private adapter: ProductAdapter) { }


  /**
   * Retrieve all products from the server.
   */
  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(this._productsUrl).pipe(
      map((data: any[]) => {
        this._fetchedProducts = data.map(productDTO => this.adapter.adapt(productDTO));
        return this._fetchedProducts;
      }),
      catchError(error => {
        // TODO : log error
        this._fetchedProducts = [];
        return of(this._fetchedProducts);
      })
    );
  }

  /**
   * Apply filterData to fetchedProducts
   * @param filterData 
   * @returns products list with products that match the given filters
   */
  filterProducts(filterData: ProductFilterData): Product[] {
    return this._fetchedProducts.filter(
      product => this.filterProduct(product, filterData)
    );
  }

  /**
   * Get a product by id
   * @param productId 
   */
  getProduct(productId: string): Observable<Product> {
    if (productId == null) {
      return of(new Product());
    }
    return this.http.get<Product>(`${this._productsUrl}/${productId}`).pipe(
      map( (productDTO: any) => this.adapter.adapt(productDTO)),
      catchError( error => { return of(null) })
    );
  }


  /**
   * Determine if product matches the given filter
   * @param product 
   * @param filter 
   * @returns true, if it matches; false otherwise
   */
  private filterProduct(product: Product, filter: ProductFilterData): boolean {
    if (filter == undefined || filter == null) {
      return true;
    }

    let zonesFilter = true;
    if (filter.zones.length > 0) {      
      zonesFilter = product.stock.filter(
                      stockElement => filter.zones.map(zone => zone.id).includes(stockElement.zone.id)
                    ).length > 0;
    }
    
    let suppliersFilter = true;
    if (filter.suppliers.length > 0) {
      suppliersFilter = product.suppliers.filter(
                          element => filter.suppliers.map(supplier => supplier.id).includes(element.supplier.id)
                        ).length > 0;
    }

    let stockFilter = product.getStockTotal() >= filter.stockRange.lower 
                      && product.getStockTotal() <= filter.stockRange.upper;

    
    let priceInRange = product.suppliers.find(productSupplier => 
      productSupplier.listPrice >= filter.priceRange.lower
      && productSupplier.listPrice <= filter.priceRange.upper
    );
    let priceFilter = priceInRange !== undefined || (product.suppliers.length == 0 && filter.priceRange.lower == 0);
    
    return zonesFilter && suppliersFilter && stockFilter && priceFilter;
  }

  /**
   * Save product suppliers 
   * @param product 
   */
  saveSuppliers(product: Product): Observable<any> {
    return this.http.post(`${this._productsUrl}/${product.id}/suppliers`, product.suppliers).pipe(
      catchError( error => { return of({status: false, error: error}) })
    );
  }

}
