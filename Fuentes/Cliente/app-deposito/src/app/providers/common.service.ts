import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(
    private toastController: ToastController
  ) { }

  async presentToast(message: string, color: string, duration = 2000, position = "bottom") {
    const toast = await this.toastController.create({
      color: color,
      message: message,
      position: position == "top" ? "top" : (position == "middle") ? "middle" : "bottom",
      duration: duration,
    });

    toast.present();
  }

  compareWithFn = (o1, o2) => {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  };

}
