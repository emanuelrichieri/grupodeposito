import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StockService {

  stockUrl = `${environment.apiUrl}/stocks`;

  constructor(private http: HttpClient) { }

  /**
   * Update stock 
   * @param stock 
   */
  updateStock(stock: {idProduct: number, idZone: number, quantity: number}[]): Observable<any> {
    return this.http.post(`${this.stockUrl}`, stock).pipe(
      catchError( error => { return of({status: false, error: error}) })
    );
  }
}
