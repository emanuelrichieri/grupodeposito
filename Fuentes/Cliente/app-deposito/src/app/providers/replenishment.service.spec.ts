import { TestBed } from '@angular/core/testing';

import { ReplenishmentService } from './replenishment.service';

describe('ReplenishmentService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReplenishmentService = TestBed.get(ReplenishmentService);
    expect(service).toBeTruthy();
  });
});
