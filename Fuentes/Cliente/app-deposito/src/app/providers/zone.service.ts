import { Injectable } from '@angular/core';
import { Zone } from '../model/zone/zone';
import { ZoneAdapter } from '../model/zone/zone-adapter';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment.prod';
import { LocationService } from './location.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@Injectable({
  providedIn: 'root'
})
export class ZoneService {

  private _zonesUrl = `${environment.apiUrl}/zones`;
  private _fetchedZones: Zone[] = [];

  geolocationSuscription;
  currentPosition: {
    latitude: number,
    longitude: number
  }

  constructor(private http: HttpClient,
              private zoneAdapter: ZoneAdapter,
              private locationService: LocationService,
              private geolocation: Geolocation) { 

    this.geolocationSuscription = this.geolocation.watchPosition().subscribe(
      position => {
        this.currentPosition = {
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        }
      }
    );
  }

  getZones(): Observable<Zone[]> {
    return this.http.get<Zone[]>(this._zonesUrl).pipe(
      map((data: any[]) => {
        this._fetchedZones = data.map(item => this.zoneAdapter.adapt(item));;
        return this._fetchedZones;
      }),
      catchError(error => {
        // TODO : log error
        this._fetchedZones= [];
        return of(this._fetchedZones);
      })
    );
  }

  getZone(zoneID: string): Observable<Zone> {
    if ( zoneID == null) {
      return of(new Zone());
    }
    return this.http.get<Zone>(`${this._zonesUrl}/${zoneID}`).pipe(
      map( (zoneDTO: any) => this.zoneAdapter.adapt(zoneDTO)),
      catchError( error => { return of(null) })
    );
  }

  suggestPath(zones: Zone[]): Zone[] {
      let nearestZone = this.getSortedByDistance(zones)[0];
      return this.locationService.calculateRoute(zones, nearestZone);
  }

  getSortedByDistance(zones: Zone[]): Zone[] {
    if (this.currentPosition) {
      return zones.sort(this.compareZonesByDistance);
    }
    return zones;
  }

  compareZonesByDistance = (zoneA: Zone, zoneB: Zone) => {
    let distanceA = this.locationService.calculateDistance(zoneA.position, this.currentPosition);
    let distanceB = this.locationService.calculateDistance(zoneB.position, this.currentPosition);
    return distanceA - distanceB;
  };
}
