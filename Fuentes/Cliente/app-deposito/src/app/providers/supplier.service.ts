import { Injectable } from '@angular/core';
import { Supplier } from '../model/supplier/supplier';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { SupplierAdapter } from '../model/supplier/supplier-adapter';
import { SupplierDTO } from '../model/supplier/supplierDTO';
import { environment } from 'src/environments/environment';
import { ProductService } from './product.service';

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  private _suppliersUrl = `${environment.apiUrl}/suppliers`;
  private _fetchedSuppliers: Supplier[] = [];

  constructor(
    private http: HttpClient,
    private supplierAdapter: SupplierAdapter,
    private productService: ProductService
  ) { }

  getSuppliers(): Observable<Supplier[]> {
    return this.http.get<Supplier[]>(this._suppliersUrl).pipe(
      map((data: any[]) => {
        this._fetchedSuppliers = data.map(item => this.supplierAdapter.adapt(item));
        return this._fetchedSuppliers;
      }),
      catchError(error => {
        // TODO : log error
        this._fetchedSuppliers = [];
        return of(this._fetchedSuppliers);
      })
    );
  }

  filterSuppliers(filterData: { status: string }): Supplier[] {
    return this._fetchedSuppliers.filter(
      supplier => {
        if (filterData.status == 'active') {
          return supplier.active == true;
        }
        if (filterData.status == 'inactive') {
          return supplier.active == false;
        }
        return true;
      }
    );
  }

  saveSupplier(supplier: Supplier): Observable<any> {
    if (supplier.id == null) {
      return this.addSupplier(supplier);
    } else {
      return this.updateSupplier(supplier);
    }
  }

  addSupplier(supplier: Supplier): Observable<any> {
    let supplierDTO: SupplierDTO = this.supplierAdapter.transform(supplier);
    return this.http.post(this._suppliersUrl, supplierDTO).pipe(
      catchError( error => { return of({status: false, error: error}) })
    );
  }

  updateSupplier(supplier: Supplier): Observable<any> {
    let supplierDTO: SupplierDTO = this.supplierAdapter.transform(supplier);
    return this.http.put(`${this._suppliersUrl}/${supplier.id}`, supplierDTO).pipe(
      catchError( error => { return of({status: false, error: error}) })
    );
  }

  getSupplier(supplierId: string): Observable<Supplier> {
    if (supplierId == null) {
      return of(new Supplier());
    }
    return this.http.get<Supplier>(`${this._suppliersUrl}/${supplierId}`).pipe(
      map( (supplierDTO: any) => this.supplierAdapter.adapt(supplierDTO)),
      catchError( error => { return of(null) })
    );
  }
}
