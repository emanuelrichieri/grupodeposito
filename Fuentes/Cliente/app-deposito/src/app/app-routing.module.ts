import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/products',
    pathMatch: 'full'
  },
  {
    path: 'home',
    redirectTo: '/products',
    pathMatch: 'full'
  },
  {
    path: 'suppliers',
    loadChildren: () => import('./pages/suppliers/suppliers.module').then(m => m.SuppliersPageModule)
  },
  {
    path: 'products',
    loadChildren: () => import('./pages/products/products.module').then(m => m.ProductsPageModule)
  },
  {
    path: 'zones',
    loadChildren: () => import('./pages/zones/zones.module').then(m => m.ZonesPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'replenishments',
    loadChildren: () => import('./pages/replenishments/replenishments.module').then(m => m.ReplenishmentsPageModule)
  },
  
  {path: 'stock',
    loadChildren: () => import('./pages/update-stock/update-stock.module').then(m => m.UpdateStockPageModule)
  },
  { 
    path: 'suggest-path', 
    loadChildren: () => import('./pages/suggest-path/suggest-path.module').then(m => m.SuggestPathPageModule)
  }

];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
