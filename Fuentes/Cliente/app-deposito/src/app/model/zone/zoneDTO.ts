export interface ZoneDTO {
    id: number;
    storeNumber: number;
    corridor: number;
    side: string;
    cabinet: number;
    shelf: number;
    position: {
        latitude: number;
        longitude: number;
    };
    stock: {
        idProduct: number, 
        code: string,
        name: string,
        description: string,
        brand: string,
        image: string,
        minimalStock: number,
        quantity: number
    }[];
}
