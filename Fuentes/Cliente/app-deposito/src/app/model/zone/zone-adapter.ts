import { Adapter } from '../adapter';
import { Zone } from './zone';
import { ZoneDTO } from './zoneDTO';
import { Injectable } from '@angular/core';
import { Product } from '../product/product';

@Injectable({
    providedIn: 'root'
})
export class ZoneAdapter implements Adapter<Zone> {

    adapt(zoneDTO: ZoneDTO): Zone {
        let zone = new Zone(zoneDTO);

        zone.stock = [];
        zoneDTO.stock.forEach(element => {
            let product = new Product(element);
            zone.stock.push({
                product: product, 
                quantity: element.quantity
            });
        });
        
        return zone;
    }

    transform(zone: Zone): ZoneDTO {
        let zoneDTO: ZoneDTO = {
            id: zone.id,
            storeNumber: zone.storeNumber,
            corridor: zone.corridor,
            side: zone.side,
            cabinet: zone.cabinet,
            shelf: zone.shelf,
            position: zone.position,
            stock: zone.stock.map(element => {
                let object = {
                    idProduct: element.product.id,
                    code: element.product.code,
                    name: element.product.name,
                    description: element.product.description,
                    brand: element.product.brand,
                    image: element.product.image,
                    minimalStock: element.product.minimalStock,
                    quantity: element.quantity
                };
                return object;
            })
        };
        return zoneDTO;
    }

}
