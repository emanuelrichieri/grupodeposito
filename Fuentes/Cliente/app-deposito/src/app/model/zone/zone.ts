import { Product } from '../product/product';

export class Zone {
    id: number;
    storeNumber: number;
    corridor: number;
    side: string;
    cabinet: number;
    shelf: number;
    position: {
        latitude: number;
        longitude: number;
    };
    stock: {
        product: Product,
        quantity: number
    }[];

    constructor();
    constructor(object: any);
    constructor (object?: any) {
        this.id = object ? (object.id ? object.id : object.idZone ? object.idZone : null): null;
        this.storeNumber = object && object.storeNumber ? object.storeNumber : null;
        this.corridor = object && object.corridor ? object.corridor : null;
        this.side = object && object.side ? object.side : null;
        this.cabinet = object && object.cabinet ? object.cabinet : null;
        this.shelf = object && object.shelf ? object.shelf : null;
        this.position = object && object.position ? object.position : null;
        this.stock = [];
    }

    getDescription(): string {
        return "Zona N°" + this.id;
    }

    getCriticalStock(): {
        product: Product,
        quantity: number
    }[] {
        return this.stock.filter(element => element.quantity < element.product.minimalStock);
    } 
}