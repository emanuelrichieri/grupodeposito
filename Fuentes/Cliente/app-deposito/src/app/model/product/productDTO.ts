export interface ProductDTO {
    id: number,
    code: string,
    name: string,
    description: string,
    brand: string,
    image: string,
    minimalStock: number,
    stock: {
        idZone: number, 
        storeNumber: number,
        corridor: number,
        side: string,
        cabinet: number,
        shelf: number,
        position: {
            latitude: number,
            longitude: number
        },
        quantity: number,

    }[],
    suppliers: {
        idSupplier: number,    
        cuit: string,
        name: string,
        telephone: string,
        address: string,
        email: string,
        active: boolean, 
        costPrice: number, 
        listPrice: number
    }[]
}