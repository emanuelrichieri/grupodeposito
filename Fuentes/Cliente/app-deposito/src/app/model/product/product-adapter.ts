import { Adapter } from '../adapter';
import { Product } from './product';
import { ProductDTO } from './productDTO';
import { Injectable } from '@angular/core';
import { Supplier } from '../supplier/supplier';
import { Zone } from '../zone/zone';

@Injectable({
    providedIn: 'root'
})
export class ProductAdapter implements Adapter<Product> {

    adapt(productDTO: ProductDTO): Product {
        let product = new Product(productDTO);

        product.stock = [];
        productDTO.stock.forEach(element => {
            let zone = new Zone(element);
            product.stock.push({zone: zone, quantity: element.quantity});
        });
        
        product.suppliers = [];
        productDTO.suppliers.forEach(element => {
            let supplier = new Supplier(element);
            product.suppliers.push({supplier: supplier, costPrice: element.costPrice, listPrice: element.listPrice});
        })

        return product;
    }

    transform(product: Product): ProductDTO {
        let productDTO: ProductDTO = {
            id: product.id,
            name: product.name,
            code: product.code,
            description: product.description,
            image: product.description,
            minimalStock: product.minimalStock,
            brand: product.brand,
            stock: product.stock.map(element => {
                let object = {
                    idZone: element.zone.id,
                    storeNumber: element.zone.storeNumber,
                    corridor: element.zone.corridor,
                    cabinet: element.zone.cabinet,
                    side: element.zone.side,
                    shelf: element.zone.shelf,
                    position: element.zone.position,
                    quantity: element.quantity
                };
                return object;
            }),
            suppliers: product.suppliers.map(element => {
                let object = {
                    idSupplier: element.supplier.id,           
                    cuit: element.supplier.cuit,
                    name: element.supplier.name,
                    telephone: element.supplier.phone,
                    address: element.supplier.address,
                    email: element.supplier.email,
                    active: element.supplier.active, 
                    costPrice: element.costPrice, 
                    listPrice: element.listPrice
                };
                return object;
            })
        };
        return productDTO;
    }

}
