import { Supplier } from '../supplier/supplier';
import { Zone } from '../zone/zone';

export class Product {
    id: number;
    code: string;
    name: string;
    description: string;
    brand: string;
    image: string;
    minimalStock: number;
    stock: {
        zone: Zone, 
        quantity: number
    }[];
    suppliers: {
        supplier: Supplier, 
        costPrice: number, 
        listPrice: number
    }[];

    constructor();
    constructor(object: any);
    constructor(object?: any) {
        this.id = object ? (object.id ? object.id : object.idProduct ? object.idProduct : null): null;
        this.code = object && object.code ? object.code : null;
        this.name = object && object.name ? object.name : null;
        this.description = object && object.description ? object.description : null;
        this.brand = object && object.brand ? object.brand : null;
        this.image = object && object.image ? object.image : null;
        this.minimalStock = object && object.minimalStock ? object.minimalStock : null;
        this.stock = [];
        this.suppliers = [];
    }

    public getStockTotal(): number {
        let stockTotal = 0;
        if (this.stock != null && this.stock != undefined) {
            this.stock.forEach(
                itemStock => {
                    stockTotal += itemStock.quantity;
                }
            )
        }
        return stockTotal;
    }

    public getDescription(): string {
        if(this.code && this.name) {
            return this.code + ' ' + this.name;
        }
        return "";
    }
}