import { Supplier } from '../supplier/supplier';
import { Zone } from '../zone/zone';

export interface ProductFilterData {
    suppliers: Supplier[], 
    zones: Zone[], 
    stockRange: {
      lower: number, 
      upper: number
    },
    priceRange: {
      lower: number,
      upper: number
    }
}