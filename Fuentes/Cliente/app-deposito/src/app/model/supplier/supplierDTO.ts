export interface SupplierDTO {
    id: number;
    cuit: string;
    name: string;
    telephone: string;
    address: string;
    email: string;
    active: boolean;
    products: {
        idProduct: number, 
        code: string,
        name: string,
        description: string,
        brand: string,
        image: string,
        costPrice: number, 
        listPrice: number
    }[]
}
