import { Product } from '../product/product';

export class Supplier {
    id: number;
    cuit: string;
    name: string;
    phone: string;
    address: string;
    email: string;
    active: boolean;
    products: {
        product: Product, 
        costPrice: number, 
        listPrice: number
    }[];

    constructor();
    constructor(object: any);
    constructor(object?: any) {
        this.id = object ? (object.id ? object.id : object.idSupplier ? object.idSupplier : null): null;
        this.cuit = object && object.cuit ? object.cuit : "";
        this.name = object && object.name ? object.name : "";
        this.phone = object && object.phone ? object.phone : "";
        this.email = object && object.email ? object.email : "";
        this.address = object && object.address ? object.address : "";
        this.active = object && object.active ? true : false;
        this.products = [];
    }
}
