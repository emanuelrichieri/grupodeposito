import { Adapter } from '../adapter';
import { Supplier } from './supplier';
import { SupplierDTO } from './supplierDTO';
import { Injectable } from '@angular/core';
import { Product } from '../product/product';

@Injectable({
    providedIn: 'root'
})
export class SupplierAdapter implements Adapter<Supplier> {

    adapt(supplierDTO: SupplierDTO): Supplier {
        let supplier = new Supplier();
        supplier.id = supplierDTO.id;
        supplier.name = supplierDTO.name;
        supplier.cuit = supplierDTO.cuit;
        supplier.address = supplierDTO.address;
        supplier.phone = supplierDTO.telephone;
        supplier.email = supplierDTO.email;
        supplier.active = supplierDTO.active;

        supplier.products = [];
        supplierDTO.products.forEach(element => {
            let product = new Product(element);
            supplier.products.push({
                product: product, 
                costPrice: element.costPrice, 
                listPrice: element.listPrice
            });
        });
        
        return supplier;
    }

    transform(supplier: Supplier): SupplierDTO {
        let supplierDTO: SupplierDTO = {
            id: supplier.id,
            cuit: supplier.cuit,
            name: supplier.name,
            telephone: supplier.phone,
            address: supplier.address,
            email: supplier.email,
            active: supplier.active,
            products: supplier.products.map(element => {
                let object = {
                    idProduct: element.product.id,
                    code: element.product.code,
                    name: element.product.name,
                    description: element.product.description,
                    brand: element.product.brand,
                    image: element.product.image,
                    costPrice: element.costPrice,
                    listPrice: element.listPrice
                };
                return object;
            })
        };
        return supplierDTO;
    }

}
