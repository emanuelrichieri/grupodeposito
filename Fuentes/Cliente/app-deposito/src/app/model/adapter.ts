export interface Adapter<T> {
    adapt(object: any): T;
    transform(object: T): any;
}
