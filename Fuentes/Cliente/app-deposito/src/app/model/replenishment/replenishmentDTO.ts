export interface ReplenishmentDTO{
    id:number;
    date: string;
    status: string;
    idSupplier: number;
    cuit: string,
    name: string,
    telephone: string,
    address: string,
    email: string,
    lines: {
        idProduct: number,
        code: string,
        name: string,
        description: string,
        brand: string,
        image: string,
        quantity: number
    }[];
}