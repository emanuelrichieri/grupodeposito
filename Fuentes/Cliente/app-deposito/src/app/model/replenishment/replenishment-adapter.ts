import { Adapter } from '../adapter';
import { Replenishment } from './replenishment';
import { ReplenishmentDTO } from './replenishmentDTO';
import { Injectable } from '@angular/core';
import { Product } from '../product/product';
import { Supplier } from '../supplier/supplier';
import { formatDate } from '@angular/common';

@Injectable({
    providedIn: 'root'
})
export class ReplenishmentAdapter implements Adapter<Replenishment> {

    adapt(replenishmentDTO: ReplenishmentDTO): Replenishment {
        let replenishment = new Replenishment(replenishmentDTO);
        replenishment.supplier = new Supplier({
            idSupplier: replenishmentDTO.idSupplier,
            cuit: replenishmentDTO.cuit,
            name: replenishmentDTO.name,
            phone: replenishmentDTO.telephone,
            email: replenishmentDTO.email,
            address: replenishmentDTO.address
        }),
        replenishment.lines= [],
        replenishmentDTO.lines.forEach(element => {
            let product = new Product(element);
            replenishment.lines.push({
                product: product,
                quantity: element.quantity,
            });
        });

        return replenishment;
    }

    transform(replenishment: Replenishment): ReplenishmentDTO {
        let replenishmentDTO: ReplenishmentDTO = {
            id: replenishment.id,
            date: formatDate(replenishment.date, 'yyyy-MM-dd', 'en'),
            status: replenishment.status,
            idSupplier: replenishment.supplier.id,
            cuit: replenishment.supplier.cuit,
            name: replenishment.supplier.name,
            telephone: replenishment.supplier.phone,
            address: replenishment.supplier.address,
            email:replenishment.supplier.email,
            lines: replenishment.lines.map(element => {
                let object = {
                    idProduct: element.product.id,
                    code: element.product.code,
                    name: element.product.name,
                    description: element.product.description,
                    brand: element.product.brand,
                    image: element.product.image,
                    quantity: element.quantity
                };
                return object;
            })
        };
        return replenishmentDTO;
    }

}