import {Supplier} from '../supplier/supplier'
import { Product } from '../product/product';
import { ReplenishmentStatus } from 'src/app/providers/replenishment.service';

export class Replenishment{
    id:number;
    date: Date;
    status: string;
    supplier: Supplier;
    lines: {product:Product,quantity: number}[];

    constructor();
    constructor(object: any);
    constructor(object?: any) {
        this.id = object ? (object.id ? object.id : object.idReplenishment ? object.isReplenishment : null): null;
        
        this.date = new Date();
        try {
            if (object.date && object.date.length == 10) {
                let dateString: string = object.date;
                let year = dateString.substr(0, 4);
                let month = dateString.substr(5, 2);
                let day = dateString.substr(8, 2);

                this.date.setFullYear(+year, +month - 1, +day);
            }
        } catch (ex) {
            this.date = new Date();
        }        
        this.date.setHours(0, 0, 0, 0);
        
        this.status = object && object.status ? object.status : ReplenishmentStatus.PENDING;
        this.lines = [];
    }

    getNumber(): string {
        if (this.id) {
            let number: string = this.id + "";
            return number.padStart(10, '0');
        }
        return "";
    }

    getStatusColor(): string {
        if (this.status == ReplenishmentStatus.PENDING) {
            return "";
        }
        if (this.status == ReplenishmentStatus.FULFILLED) {
            return "success";
        }
        if (this.status == ReplenishmentStatus.OVERRIDEN) {
            return "danger";
        }
        if (this.status == ReplenishmentStatus.REJECTED) {
            return "warning";
        }
        if (this.status == ReplenishmentStatus.ISSUED) {
            return "secondary";
        }
        if (this.status == ReplenishmentStatus.IN_PROGRESS) {
            return "tertiary";
        }
        if (this.status == ReplenishmentStatus.APPROVED) {
            return "primary";
        }
    }
}