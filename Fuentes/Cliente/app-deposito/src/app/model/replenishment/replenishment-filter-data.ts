import { Supplier } from '../supplier/supplier';
import { Product } from '../product/product';

export interface ReplenishmentFilterData {
    status: string[],
    suppliers: Supplier[], 
    products: Product[],
    dateFrom: Date, 
    dateTo: Date
}